<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Controller/BoutiqueController.php

namespace App\Controller;
use App\Entity\Product;
use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
class BoutiqueController extends AbstractController {
    
    public function index() {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository(Category::class)->findAll();
        return $this->render("boutique_index.html.twig", ["categories" => $categories]);
    }

    public function category($idCategorie) {
        $em = $this->getDoctrine()->getManager();
        $categorie = $em->getRepository(Category::class)->find($idCategorie);
        $produits = $categorie->getProducts();
        return $this->render('boutique_category.html.twig', ['produits' => $produits]);
    }
    
    public function product($idProduct) {
        $em = $this->getDoctrine()->getManager();
        $produit = $em->getRepository(Product::class)->find($idProduct);
        return $this->render('boutique_product.html.twig', ['produit' => $produit]);
    }
}