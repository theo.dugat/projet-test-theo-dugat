<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Service\PanierService;
class PanierController extends AbstractController {
    
    public function index(PanierService $ps) {
        $produits = $ps->getContenu();
        if(!$produits==[]){
            //$total = $ps->getTotal();
            return $this->render("panier/index.html.twig", ["produits" => $produits]);
        }else{
            return $this->render("panier/index.html.twig");
        }
    }
    
    public function ajouter(int $idProduit, PanierService $ps, int $quantite) {
        //var_dump($idProduit);
        $ps->ajouterProduit($idProduit,$quantite);
        return $this->redirectToRoute('panier_index');
    }
    
    public function enlever(PanierService $ps, int $id, int $quantite=1) {
        $ps->enleverProduit($id,$quantite);
        return $this->redirectToRoute('panier_index');
    }
    
    public function supprimer(PanierService $ps, int $id) {
        $ps->supprimerProduit($id);
        return $this->redirectToRoute('panier_index');
    }
    
    public function vider(PanierService $ps) {
        $ps->vider();
        return $this->redirectToRoute('panier_index');
    }
}