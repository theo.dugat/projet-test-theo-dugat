<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// src/Service/PanierService.php
namespace App\Service;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Product;
use App\Service\BoutiqueService;
// Service pour manipuler le panier et le stocker en session
class PanierService {
    ////////////////////////////////////////////////////////////////////////////
    const PANIER_SESSION = 'panier'; // Le nom de la variable de session du panier
    private $session; // Le service Session
    private $em; // EntityManager
    private $boutique; // Le service Boutique
    private $panier; // Tableau associatif idProduit => quantite
    // donc $this->panier[$i] = quantite du produit dont l'id = $i
    // constructeur du service
    public function __construct(SessionInterface $session, BoutiqueService $boutique, EntityManagerInterface $em) {
        // Récupération des services session et BoutiqueService
        $this->boutique = $boutique;
        $this->session = $session;
        $this->em = $em;
        //Lancement de la session
        $this->session->start();
        // Récupération du panier en session s'il existe, init. à vide sinon
        $this->panier = $this->session->get(PanierService::PANIER_SESSION, []); // Initialisation du Panier
    }
    // getContenu renvoie le contenu du panier
    // tableau d'éléments [ "produit" => un produit, "quantite" => quantite ]
    public function getContenu() {
        $repository = $this->em->getRepository(Product::class);
        $res = [];
        //var_dump($this->panier);
        foreach ($this->panier as $id => $quantite) {
            $produit = $repository->find($id);
            $res[$id] = ["produit" => $produit, "quantite" => $quantite];
        }
        //var_dump($res);
        return $res;
    }
    
    // getTotal renvoie le montant total du panier
    public function getTotal() {
        $repository = $this->em->getRepository(Product::class);
        $total = 0;
        foreach($this->panier as $id => $quantite){
            $produit = $repository->find($id);
            $total += $quantite * $produit->getPrix();
        }
        return $total;
    }
    
    // getNbProduits renvoie le nombre de produits dans le panier
    public function getNbProduits() {
        $total = 0;
        foreach($this->panier as $id => $quantite){
            $total += $quantite;
        }
        return $total;
    }
    // ajouterProduit ajoute au panier le produit $idProduit en quantite $quantite
    public function ajouterProduit(int $id, int $quantite = 1) {
        //var_dump($quantite);
        if (!isset($this->panier[$id])) {
            $this->panier[$id] = $quantite;
            //var_dump($this->panier[$id]);
        } else {
            $this->panier[$id] += $quantite;
            //var_dump($this->panier[$id]);
        }
        $this->session->set(PanierService::PANIER_SESSION, $this->panier);
    }
    
    // enleverProduit enlève du panier le produit $idProduit en quantite $quantite
    public function enleverProduit(int $id, int $quantite = 1) {
        if ($this->panier[$id]==1 || !isset($this->panier[$id])) {
            $this->supprimerProduit($id);
        }
        else {
            $this->panier[$id] -= $quantite;
        }
        $this->session->set(PanierService::PANIER_SESSION, $this->panier);
    }
    
    // supprimerProduit supprime complètement le produit $idProduit du panier
    public function supprimerProduit(int $id) {
        unset($this->panier[$id]);
        $this->session->set(PanierService::PANIER_SESSION, $this->panier);
    }
    // vider vide complètement le panier
    public function vider() {
        $this->panier = [];
        $this->session->set(PanierService::PANIER_SESSION, $this->panier);
    }
}