<?php

namespace App\Tests;

use App\Entity\Product;
use App\Service\PanierService;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ProductRepository;
use App\Service\BoutiqueService;
use PHPUnit\Framework\TestCase;

class PanierUnitTest extends TestCase
{
    public function testSomething(): void
    {
        $this->assertTrue(true);
    }
    
    //Méthode pour vérifier le contenu du panier
    public function testGetContenuePanier()
    {
        //Création des objetd Mock
       $session = $this->createMock(SessionInterface::class);
       $entityManager = $this->createMock(EntityManagerInterface::class);
       $product = $this->createMock(Product::class);
       $boutique_service = $this->createMock(BoutiqueService::class);
       $product_repository = $this->createMock(ProductRepository::class);

      //on rédéfini la méthode get session idProduit => quantité
      $session->method('get')->willreturn([1=>7,2=>5,3=>4]);
      $entityManager->method('getRepository')->willreturn($product_repository);
      $product_repository->method('find')->willreturn($product);

     //créer une instance de la classe PanierService
     $panierService = new PanierService($session, $boutique_service, $entityManager);


      $res = $panierService->getContenu();
      //on vérifie que le resultat de la methode getContenu n'est pas vide
      $this->assertNotEmpty($res);
    }
    
    //Méthode pour vérifier le contenu du panier
    public function testGetTotalPanier()
    {
        //Création des objetd Mock
       $session = $this->createMock(SessionInterface::class);
       $entityManager = $this->createMock(EntityManagerInterface::class);
       $product = $this->createMock(Product::class);
       $boutique_service = $this->createMock(BoutiqueService::class);
       $product_repository = $this->createMock(ProductRepository::class);

      //on rédéfini la méthode get session idProduit => quantité
      $session->method('get')->willreturn([1=>7,2=>5,3=>4]);
      $entityManager->method('getRepository')->willreturn($product_repository);
      $product_repository->method('find')->willreturn($product);

     //créer une instance de la classe PanierService
     $panierService = new PanierService($session, $boutique_service, $entityManager);


      $res = $panierService->getTotal();
      //on vérifie que le resultat de la methode
      $this->assertEquals(0, $res);
    }
    
    //Méthode pour vérifier le contenu du panier
    public function testGetNbProduitsPanier()
    {
        //Création des objetd Mock
       $session = $this->createMock(SessionInterface::class);
       $entityManager = $this->createMock(EntityManagerInterface::class);
       $product = $this->createMock(Product::class);
       $boutique_service = $this->createMock(BoutiqueService::class);
       $product_repository = $this->createMock(ProductRepository::class);

      //on rédéfini la méthode get session idProduit => quantité
      $session->method('get')->willreturn([1=>7,2=>5,3=>4]);
      $entityManager->method('getRepository')->willreturn($product_repository);
      $product_repository->method('find')->willreturn($product);

     //créer une instance de la classe PanierService
     $panierService = new PanierService($session, $boutique_service, $entityManager);


      $res = $panierService->getNbProduits();
      //on vérifie que le resultat de la method getNbProduits renvoi 16
      $this->assertEquals(16, $res);
    }
    
    //Méthode pour vérifier le contenu du panier
    public function testEnleverProduitPanier()
    {
        //Création des objetd Mock
       $session = $this->createMock(SessionInterface::class);
       $entityManager = $this->createMock(EntityManagerInterface::class);
       $product = $this->createMock(Product::class);
       $boutique_service = $this->createMock(BoutiqueService::class);
       $product_repository = $this->createMock(ProductRepository::class);

      //on rédéfini la méthode get session idProduit => quantité
      $session->method('get')->willreturn([1=>7,2=>5,3=>4]);
      $entityManager->method('getRepository')->willreturn($product_repository);
      $product_repository->method('find')->willreturn($product);

     //créer une instance de la classe PanierService
     $panierService = new PanierService($session, $boutique_service, $entityManager);


      $panierService->enleverProduit(1,2);
      $res = $panierService->getNbProduits();
      //on vérifie que le nombre de produit est egal à 16
      $this->assertEquals(14, $res);
    }
    
    //Méthode pour vérifier le contenu du panier
    public function testSupprimerProduitPanier()
    {
        //Création des objetd Mock
       $session = $this->createMock(SessionInterface::class);
       $entityManager = $this->createMock(EntityManagerInterface::class);
       $product = $this->createMock(Product::class);
       $boutique_service = $this->createMock(BoutiqueService::class);
       $product_repository = $this->createMock(ProductRepository::class);

      //on rédéfini la méthode get session idProduit => quantité
      $session->method('get')->willreturn([1=>7,2=>5,3=>4]);
      $entityManager->method('getRepository')->willreturn($product_repository);
      $product_repository->method('find')->willreturn($product);

     //créer une instance de la classe PanierService
     $panierService = new PanierService($session, $boutique_service, $entityManager);


     $panierService->ajouterProduit(2,2);
     $res = $panierService->getNbProduits();
      //on vérifie que le nombre de produit est egal à 18
      $this->assertEquals(18, $res);
    }
    
    //Méthode pour vérifier le contenu du panier
    public function testViderProduitPanier()
    {
        //Création des objetd Mock
       $session = $this->createMock(SessionInterface::class);
       $entityManager = $this->createMock(EntityManagerInterface::class);
       $product = $this->createMock(Product::class);
       $boutique_service = $this->createMock(BoutiqueService::class);
       $product_repository = $this->createMock(ProductRepository::class);

      //on rédéfini la méthode get session idProduit => quantité
      $session->method('get')->willreturn([1=>7,2=>5,3=>4]);
      $entityManager->method('getRepository')->willreturn($product_repository);
      $product_repository->method('find')->willreturn($product);

     //créer une instance de la classe PanierService
     $panierService = new PanierService($session, $boutique_service, $entityManager);


      $res = $panierService->vider();
      $res = $panierService->getNbProduits();
      //on vérifie que le nombre de produit est egal à 0
      $this->assertEquals(0, $res);
    }
}
