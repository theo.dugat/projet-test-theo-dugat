<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PanierWebTest extends WebTestCase
{
    public function testSomething(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        #$this->assertSelectorTextContains('h1', 'Hello World');
    }

    public function urlProvider()
    {
        yield ['/'];
        yield ['/boutique'];
        yield ['/boutique/1'];
        yield ['/boutique/2'];
        yield ['/boutique/3'];
        yield ['/panier'];
        yield ['/panier/ajouter/1/1'];
        yield ['/panier/enlever/1/1'];
        yield ['/panier/enlever/1/2'];
        yield ['/panier/enlever/3/2'];
        yield ['/panier/vider'];
    }
}
